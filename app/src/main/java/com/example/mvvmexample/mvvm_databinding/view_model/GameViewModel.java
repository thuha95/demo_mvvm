package com.example.mvvmexample.mvvm_databinding.view_model;

import androidx.databinding.ObservableArrayMap;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.mvvmexample.R;
import com.example.mvvmexample.mvvm_databinding.model.Cell;
import com.example.mvvmexample.mvvm_databinding.model.Manager;
import com.example.mvvmexample.mvvm_databinding.model.Player;


public class GameViewModel extends ViewModel {
    private Manager manager;
    public ObservableArrayMap<String, Integer> cells; //một map lưu vị trí của ô trên bàn cờ và giá trị tại ô đó

    public void init (String p1, String p2){
        manager = new Manager(p1, p2);
        cells = new ObservableArrayMap<>();
    }

    public void onClickAtCell (int r, int c) {
        if (manager.getCells()[r][c] == null){
            manager.getCells()[r][c] = new Cell(manager.getCurPlayer());
            int res = 0;
            if (manager.getCurPlayer().getValue() == Player.PlayerValue.PLAYER_VALUE_X) {
                res = R.drawable.ic_success;
            } else {
                res = R.drawable.unselected_b2c_gray;
            }

            cells.put(String.format("%s%s",r,c), res);
            // mỗi khi một ô được chọn sẽ kiểm tra xem game kết thúc chưa? nếu rồi thì toast thông báo, không thì đổi lượt chơi
            if (manager.isGameEnded()){
                resetGame();
            } else {
                manager.switchPlayer();
            }
        }
    }

    public ObservableArrayMap<String, Integer> getCells() {
        return cells;
    }

    private void resetGame() {
        manager.reset();
        cells.clear();
    }

    public LiveData<Player> getWinner(){
        return manager.getWinner();
    }
}
