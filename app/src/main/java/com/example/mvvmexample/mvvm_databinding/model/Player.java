package com.example.mvvmexample.mvvm_databinding.model;

public class Player {
    private String name;
    private PlayerValue value;

    public Player(String name, PlayerValue value) {
        this.name = name;
        this.value = value;
    }

    public Player(String name, String value) {
        this.name = name;
        if (value.equalsIgnoreCase("x")) {
            this.value = PlayerValue.PLAYER_VALUE_X;
        } else if (value.equalsIgnoreCase("o")) {
            this.value = PlayerValue.PLAYER_VALUE_O;
        } else {
            this.value = PlayerValue.PLAYER_VALUE_EMPTY;
        }
    }

    public String getName() {
        return name;
    }

    public PlayerValue getValue() {
        return value;
    }

    public enum PlayerValue {
        PLAYER_VALUE_X("X"),
        PLAYER_VALUE_O("O"),
        PLAYER_VALUE_EMPTY("EMPTY");

        private String value;

        PlayerValue(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }
}
