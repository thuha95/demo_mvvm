package com.example.mvvmexample.mvvm_databinding.view;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.VisibleForTesting;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.mvvmexample.R;
import com.example.mvvmexample.databinding.FrgGameXoBinding;
import com.example.mvvmexample.mvvm_databinding.model.Player;
import com.example.mvvmexample.mvvm_databinding.view_model.GameViewModel;

public class GameOXFragment extends AppCompatActivity {
    private static final String NO_WINNER = "NO_WINNER" ;
    private GameViewModel gameViewModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FrgGameXoBinding frgGameXoBinding = DataBindingUtil.setContentView(this, R.layout.frg_game_xo);
        gameViewModel = ViewModelProviders.of(this).get(GameViewModel.class);
        gameViewModel.init("P1", "P2");
        frgGameXoBinding.setGameViewModel(gameViewModel);
        setUpGameEndListener();
    }

    private void setUpGameEndListener() {
        //đăng kí lắng nghe sự kiện khi nào trò chơi kết thúc
        gameViewModel.getWinner().observe(this, new Observer<Player>() {
            @Override
            public void onChanged(Player player) {
                onGameWinnerChange(player);
            }
        });
    }

    @VisibleForTesting
    public void onGameWinnerChange(Player winner) {
        String winnerName = (winner != null &&
                (winner.getName() != null && !winner.getName().isEmpty())) ? winner.getName() : NO_WINNER;
        Toast.makeText(this, "Winner is " + winnerName, Toast.LENGTH_SHORT).show();
    }
}
