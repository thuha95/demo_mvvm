package com.example.mvvmexample.mvvm_databinding.model;

public class Cell {
    private Player player;

    public Cell(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public Cell(String name, String value) {
        this.player = new Player(name, value);
    }

    public boolean isEmpty() {
        return player == null || player.getValue() == Player.PlayerValue.PLAYER_VALUE_EMPTY;
    }
}
