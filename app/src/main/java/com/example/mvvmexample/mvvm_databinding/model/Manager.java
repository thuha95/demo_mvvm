package com.example.mvvmexample.mvvm_databinding.model;

import androidx.lifecycle.MutableLiveData;
//Đối tượng quản lý hai ng chơi và bàn cờ
public class Manager {
    public static final String TAG = Manager.class.getName();
    public static final int BOARD_SIZE = 3;

    private Cell[][] cells;
    private Player player1;
    private Player player2;
    private Player curPlayer;

    private MutableLiveData<Player> winner = new MutableLiveData<>();

    public Manager(String player1, String player2) {
        cells = new Cell[BOARD_SIZE][BOARD_SIZE];
        this.player1 = new Player(player1, "x");
        this.player2 = new Player(player2, "o");

        curPlayer = this.player1;
    }

    public Cell[][] getCells() {
        return cells;
    }

    public Player getCurPlayer() {
        return curPlayer;
    }

    public void switchPlayer() {
        curPlayer = curPlayer == player1 ? player2 : player1;
    }

    public boolean isGameEnded() {
        // check hàng dọc, ngang, chéo nếu có ba phần tử giống nhau thì return true
        if (hasThreeSameOnHorizontalCells() || hasThreeSameOnVerticalCells()
                || hasThreeSameOnDiagonalCells()) {
            winner.setValue(curPlayer);
            return true;
        }
        // nếu bàn cờ kín return true
        if (isBoardGameFull()) {
            winner.setValue(null);
            return true;
        }
        return false;
    }

    private boolean hasThreeSameOnVerticalCells() {
        Player.PlayerValue value = curPlayer.getValue();
        return areEquals(value, cells[0][0], cells[1][0], cells[2][0]) ||
                areEquals(value, cells[0][1], cells[1][1], cells[2][1]) ||
                areEquals(value, cells[0][2], cells[1][2], cells[2][2]);
    }

    //kiểm tra các phần tử của cells có giống nhau không
    private boolean areEquals(Player.PlayerValue value, Cell... cells) {
        for (Cell cell : cells) {
            if (cell == null || cell.isEmpty() || cell.getPlayer() != curPlayer || cell.getPlayer().getValue() != value)
                return false;
        }
        return true;
    }

    private boolean hasThreeSameOnHorizontalCells() {
        Player.PlayerValue value = curPlayer.getValue();
        return areEquals(value, cells[0][0], cells[0][1], cells[0][2]) ||
                areEquals(value, cells[1][0], cells[1][1], cells[1][2]) ||
                areEquals(value, cells[2][0], cells[2][1], cells[2][2]);
    }

    private boolean hasThreeSameOnDiagonalCells() {
        Player.PlayerValue value = curPlayer.getValue();
        return areEquals(value, cells[0][0], cells[1][1], cells[2][2]) ||
                areEquals(value, cells[0][2], cells[1][1], cells[2][0]);
    }

    private boolean isBoardGameFull() {
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (cells[i][j] == null || cells[i][j].isEmpty()) return false;
            }
        }
        return true;
    }

    public void reset() {
        curPlayer = player1;
        cells = new Cell[BOARD_SIZE][BOARD_SIZE];
    }

    public MutableLiveData<Player> getWinner() {
        return winner;
    }
}
